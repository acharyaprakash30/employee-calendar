import React from "react";

const Modal = ({
  open,
  eventTitle,
  setEventTitle,
  handleSubmit,
  setEventType,
  eventType,
  setAbsentType,
  absentType,
  isEmptySlot
}) => {
  return (
    <div>
      <div className="fixed w-screen h-screen top-0 right-0 left-0 bg-black/50 z-[1000]">
        <div className="flex items-center justify-center mt-20">
          <div className="w-1/3 bg-white rounded">
            <div className="flex justify-between w-full p-6 ">
              <p className="text-2xl font-semibold">Add Event</p>

              <button
                onClick={() => open(false)}
                className="text-white bg-red-500 rounded-lg px-3 pb-1"
              >
                x
              </button>
            </div>

            <div className="p-8 flex flex-col gap-8">
              <div className="flex flex-col gap-2">
                <label htmlFor="eventTitle" className="font-medium text-base">
                  Event:
                </label>
                <input
                  type="text"
                  id="eventTitle"
                  name="eventTitle"
                  value={eventTitle}
                  onChange={(e) => setEventTitle(e.target.value)}
                  className="border border-gray-200 py-3 px-2 rounded-lg bg-gray-100 outline-none"
                />
              </div>
              <div className="flex gap-2 items-center">
                <label htmlFor="eventType" className="font-medium text-base">
                  Type:
                </label>
                <select
                  id="eventType"
                  name="eventType"
                  className="border border-gray-200 py-1 px-2 rounded-lg bg-gray-100 outline-none w-fit"
                  value={eventType}
                  onChange={(e) => setEventType(e.target.value)}
                  defaultValue="Present"
                  required
                >
                  <option>Select Type</option>
                  {
                    isEmptySlot ?(<><option value="Absent">Absent</option><option value="None">None</option></>):   <option value="Present">Present</option> 
                  }
                  {/* <option value="Present">Present</option>
                  <option value="Absent">Absent</option> */}
                
                </select>

                {eventType === "Absent" && (
                  <div className="rounded-full w-6 h-6 bg-[#e6e6e6]"></div>
                )}
                {eventType === "Present" && (
                  <div className="rounded-full w-6 h-6 bg-green-600"></div>
                )}
                {eventType === "None" && (
                  <div className="rounded-full w-6 h-6 border"></div>
                )}
              </div>
              {eventType === "Absent" && (
                <div className="flex gap-2 items-center">
                  <label htmlFor="absentType" className="font-medium text-base">
                    Absent Type:
                  </label>
                  <select
                    id="absentType"
                    name="absentType"
                    className="border border-gray-200 py-1 px-2 rounded-lg bg-gray-100 outline-none w-fit"
                    value={absentType}
                    onChange={(e) => setAbsentType(e.target.value)}
                  >
                    <option value="Sick">Sick</option>
                    <option value="Vacation">Vacation</option>
                  </select>

                  {absentType === "Sick" && (
                    <div className="rounded-full w-6 h-6 bg-[#ff0000d6]"></div>
                  )}
                  {absentType === "Vacation" && (
                    <div className="rounded-full w-6 h-6 bg-[#0000ffa3]"></div>
                  )}
                </div>
              )}

              <div className="w-full">
                <button
                  type="submit"
                  onClick={handleSubmit}
                  className="bg-blue-500 text-white py-2 px-6 rounded-xl w-fit float-right"
                >
                  Submit
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Modal;
