"use client";
import React, { useEffect, useState } from "react";
import { getBranches } from "../app/apiServices/services";

const DropDown = ({dropDownItem, handleSelect }) => {
  return (
    <select
      className="border-2 border-gray-200 outline-none rounded-md px-2 py-1 float-right"
      onChange={(e) => handleSelect(e.target.value)} 
      defaultValue="none"
    >
      <option value="none" disabled>
        Select Cost Center
      </option>
      {dropDownItem.map((item,index) => (
        <option value={item.kostenstelle} key={index}>
          {item.kostenstelle}
        </option>
      ))}
    </select>
  );
};

export default DropDown;
