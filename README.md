
## Project Setup

Clone the project

```bash
  git clone https://gitlab.com/acharyaprakash30/employee-calendar
```

Go to the project directory

```bash
  cd my-project
```

Install dependencies

```bash
  yarn install
```

Setup database in .env file 

```bash
DATABASE_URL="sqlserver://host:port;database=database_name;user=your-user-name;password=your-password;encrypt=true;trustServerCertificate=true;"
```
```bash
example:
DATABASE_URL="sqlserver://localhost:1433;database=big_calendar;user=SA;password=HelloWorld@123;encrypt=true;trustServerCertificate=true;"
```
Migrate table to database

```bash
yarn run db:push
```

Run Seeder File

```bash
node prisma/seed
```


Start the server

```bash
  yarn run dev
```






