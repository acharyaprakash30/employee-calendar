import axios from 'axios';

const url = process.env.DATABASE_URL;
const api = axios.create({
    baseURL: url
});


api.interceptors.response.use(
    async (response) => {
        if (response.status >= 200 && response.status < 300) {
            const data = response.data;
            return Promise.resolve(data);
        }
    },
    async (error) => {
        const { response } = error;
        return Promise.reject(response?.data);
    }
)
export default api;