import api from ".";

export const getEvents = async () => {
    return await api.get(`api/event`);
};

export const getBranches = async () => {
    return await api.get(`api/branch`);
};

export const getBranchById = async (id) => {
    return await api.get(`api/branch/${id}`);
};

export const createEvent = async (event) => {
    return await api.post(`api/event`, event);
};

export const updateEvent = async (eventId,event) => {
    return await api.patch(`api/event/${eventId}`, event);
};

export const getEmployees = async () => {
    return await api.get(`api/employee`);
};