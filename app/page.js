"use client";
import React, { useEffect, useReducer, useState } from "react";
import moment from "moment";
import {
  Scheduler,
  SchedulerData,
  ViewType,
  AddMorePopover,
  CellUnit,
  wrapperFun,
} from "react-big-schedule";

import Dropdown from "../components/Dropdown";
import Modal from "../components/Modal";
import {
  createEvent,
  getBranchById,
  getBranches,
  updateEvent,
} from "./apiServices/services";

let schedulerData;

const initialState = {
  showScheduler: false,
  viewModel: {},
};

function reducer(state, action) {
  switch (action.type) {
    case "INITIALIZE":
      return { showScheduler: true, viewModel: action.payload };
    case "UPDATE_SCHEDULER":
      return { ...state, viewModel: action.payload };
    default:
      return state;
  }
}

function AddMore() {
  const [state, dispatch] = useReducer(reducer, initialState);
  const [selectedID, setSelectedID] = useState("");
  const [filteredEvents, setFilteredEvents] = useState([]);
  const [branch, setBranch] = useState([]);
  // const [employee, setEmployee] = useState([]);
  const [toggleModal, setToggleModal] = useState(false);
  const [myEvents, setMyEvents] = useState([]);
  const [eventTitle, setEventTitle] = useState("");
  const [eventType, setEventType] = useState("");
  const [absentType, setAbsentType] = useState("");
  const [employeeName, setEmployeeName] = useState("");
  const [employeeID, setEmployeeID] = useState();
  const [selectedDate, setSelectedDate] = useState();
  const [endDate, setEndDate] = useState();
  const [addedEvent, setAddedEvent] = useState({});
  const [resources, setResources] = useState([]);
  const [initialEvents, setInitialEvents] = useState([]);
  const [isEmptySlot, setIsEmptySlot] = useState(false);

  useEffect(() => {
    getBranch();
  }, []);

  const getBranch = async () => {
    try {
      const res = await getBranches();
      if (res.data !== "") {
        setBranch(res.data);
      }
    } catch (error) {
      console.log("An error Occured");
    }
  };

  const [popoverState, setPopoverState] = useState({
    headerItem: undefined,
    left: 0,
    top: 0,
    height: 0,
  });

  const getCustomDateFunc = (schedulerData, num, date = undefined) => {
    let selectDate = schedulerData.startDate;
    if (date !== undefined) {
      selectDate = date;
    }

    const yearStart = moment(selectDate).startOf('year');
    const currentWeek = moment(selectDate).diff(yearStart, 'weeks');

    // Calculate the end date to limit the number of weeks displayed to 52
    const endDate = yearStart.clone().add(51, 'weeks').endOf('week');

    const startDate = yearStart.clone().add(currentWeek * 7, 'days');
    const weekStartDate = startDate.clone().startOf('week');
    const weekEndDate = endDate.clone().endOf('week');

    const cellUnit = CellUnit.Week;
    const cellWidth = 7 * schedulerData.config.customCellWidth;

    return { startDate: weekStartDate, endDate: weekEndDate, cellUnit, cellWidth };
  }

  useEffect(() => {
    schedulerData = new SchedulerData(
      "2024-01-01",
      ViewType.Custom,
      false,
      false,
      {
        views: [],
        besidesWidth: window.innerWidth <= 1600 ? 100 : 350,
        schedulerWidth: 1000,
        tableHeaderHeight: 50,
        eventItemLineHeight: 50,
        schedulerMaxHeight: 500,
        // nonWorkingTimeBodyBgColor: "#fff",
        dayMaxEvents: 2,
        weekMaxEvents: 4,
        monthMaxEvents: 4,
        quarterMaxEvents: 4,
        yearMaxEvents: 4,
      },
      {
        getCustomDateFunc
      }
    );
    schedulerData.localeDayjs.locale("en");
    schedulerData.config.resourceName = "Employee";
    dispatch({ type: "INITIALIZE", payload: schedulerData });
  }, []);

  const handleSelect = async (id) => {
    setSelectedID(id);
    const res = await getBranchById(id);
    const data = res?.data;
    // const employeesList = data && data.map((user) => user.persname);
    if (res && data) {
      const formattedJSON = [
        {
          date: new Date().toISOString(),
          time: new Date().toLocaleTimeString(),
        },
      ];
      res.data.events.map((user, index) => {
        if (
          !formattedJSON.some(
            (format) => format.personalnr === user.personalnr
          )
        ) {
          formattedJSON.push({
            kostenstelle: user.kostenstelle,
            personalnr: user.personalnr,
            persname: user.persname,
            events: [
              {
                title: user.auftragstyp,
                start: user.datumvon,
                end: user.datumbis,
                type: user.event_type,
                reason: user.reason,
              },
            ],
          });
        }
      });

      setResources(res.data.resources);
      setInitialEvents(res.data.eventData);
      setMyEvents(formattedJSON);
      schedulerData && schedulerData.setResources(res.data.resources);
      schedulerData && schedulerData.setEvents(res.data.eventData);
      dispatch({ type: "UPDATE_SCHEDULER", payload: schedulerData });
    }
  };
  const prevClick = (schedulerData) => {
    schedulerData.prev();
    schedulerData.setEvents(initialEvents);
    dispatch({ type: "UPDATE_SCHEDULER", payload: schedulerData });
  };

  const nextClick = (schedulerData) => {
    schedulerData.next();
    schedulerData.setEvents(initialEvents);
    dispatch({ type: "UPDATE_SCHEDULER", payload: schedulerData });
  };

  const onViewChange = (schedulerData, view) => {
    schedulerData.setViewType(
      view.viewType,
      view.showAgenda,
      view.isEventPerspective
    );
    schedulerData.setEvents(initialEvents);
    dispatch({ type: "UPDATE_SCHEDULER", payload: schedulerData });
  };

  const onSelectDate = (schedulerData, date) => {
    schedulerData.setDate(date);
    schedulerData.setEvents(initialEvents);
    dispatch({ type: "UPDATE_SCHEDULER", payload: schedulerData });
  };
  const getResourceId = (event) => {
    console.log(event, "event")
    let resourceId = resources.filter((item) => item.parentId == event.resourceId)[0].id;
    return resourceId
  }
  const newEvent = (
    schedulerData,
    slotId,
    slotName,
    start,
    end,
    type,
    item
  ) => {
    if (start) {
      let newFreshId = 0;
      schedulerData.events.forEach((item) => {
        if (item.id >= newFreshId) newFreshId = item.id + 1;
      });
      let newEvent = {
        id: newFreshId,
        title: eventTitle,
        start: start,
        end: end,
        resourceId: slotId,
        resourceName: slotName

      };
      setAddedEvent(newEvent);

      setToggleModal(true);

      if (slotName == null) {
        let parentSlot = resources.find((item) => item.id === resources.find((child) => child.id === slotId)?.parentId);
        newEvent.resourceName = parentSlot.name;
        newEvent.resourceId = Number(parentSlot.id)
        setIsEmptySlot(true)
      }

      // schedulerData.addEvent(newEvent);
      // dispatch({ type: "UPDATE_SCHEDULER", payload: schedulerData });
      setSelectedDate(newEvent.start);
      setEndDate(newEvent.end);
      setEmployeeID(newEvent.resourceId);
      setEmployeeName(newEvent.resourceName);
      setEventTitle(newEvent.title);

    }
  };
  const saveEvent = async () => {
    const mappedStartDate = moment(selectedDate).format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
    const mappedEndDate  = moment(selectedDate).add(30, 'minutes').format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
    const mappedEndUpdateDate  = moment(selectedDate).add(7, 'days').format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
    if (eventTitle) {
      const newEvent = {
        auftragstyp: eventTitle,
        event_type: eventType,
        reason: absentType,
        personalnr: Number(employeeID),
        kostenstelle: Number(selectedID),
        persname: employeeName,
        datumvon: mappedStartDate,
        datumbis: mappedEndUpdateDate,
      };
      const updatedNewEvent = {
        id: addedEvent.id,
        title: eventTitle,
        start: mappedStartDate,
        end: mappedEndDate,
        resourceId: eventType == "Present" ? addedEvent.resourceId : getResourceId(addedEvent),
        bgColor:
          eventType === "Present"
            ? "green"
            : absentType === "Sick"
              ? "#ff0000d6"
              : absentType === "Vacation"
                ? "#0000ffa3"
                : "gray",
      };
      const create = await createEvent(newEvent)
        .then((res) => res.data)
        .catch((err) => alert(err.message));
      if (create) {
        setToggleModal(false);
        setEventTitle("");
        setEventType("");
        schedulerData.addEvent(updatedNewEvent);
        schedulerData.setResources(resources);
        dispatch({ type: "UPDATE_SCHEDULER", payload: schedulerData });
      }
      setToggleModal(false);
    }
  };

  const updateEventStart = async (
    schedulerData,
    event,
    newStart,
    slotId,
    slotName
  ) => {
    if (schedulerData) {
      const newEvent = {
        persname: slotName,
        personalnr: slotId,
        start: new Date(newStart),
      };

      const update = await updateEvent(event.id, newEvent)
        .then((res) => res.data)
        .catch((err) => alert(err.message));

      schedulerData.updateEventStart(event, newStart);
    }
    dispatch({ type: "UPDATE_SCHEDULER", payload: schedulerData });
  };

  const updateEventEnd = async (
    schedulerData,
    event,
    newEnd,
    slotId,
    slotName
  ) => {
    
    if (schedulerData) {

      const newEvent = {
        datumbis: new Date(newEnd),
      };
      const scheduleEvent = {
        end:new Date(newEnd)
      }
      const diffInDays = moment(newEnd).diff(moment(event.start), 'days');
      const formatStartEvent = moment(event.start).startOf('day');
      
      if(diffInDays >= 0){
        console.log(diffInDays)
        newEvent.datumbis = formatStartEvent.add(7*Number(diffInDays), 'days').format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
        scheduleEvent.end = formatStartEvent.add((30*Number(diffInDays)), 'minutes').format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
      }else{
        newEvent.datumbis = formatStartEvent.subtract(7*Number(diffInDays), 'days').format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
        scheduleEvent.end = formatStartEvent.subtract(30*Number(diffInDays) + 1, 'minutes').format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
      }
      console.log(newEvent,scheduleEvent,newEnd)
      const update = await updateEvent(event.id, newEvent)
        .then((res) => res.data)
        .catch((err) => alert(err.message));


      schedulerData.updateEventEnd(event, scheduleEvent.end);
    }
    dispatch({ type: "UPDATE_SCHEDULER", payload: schedulerData });
  };

  const moveEvent = async (
    schedulerData,
    event,
    slotId,
    slotName,
    start,
    end
  ) => {
    if (schedulerData) {

      const mappedStartDate = moment(start).format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
      const mappedEndDate  = moment(start).add(30, 'minutes').format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
      const mappedEndUpdateDate  = moment(start).add(7, 'days').format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
      const newEvent = {
        persname: slotName,
        personalnr: slotId,
        datumvon: mappedStartDate,
        datumbis:mappedEndUpdateDate,
      };

      if(newEvent.persname == null){
        newEvent.persname = newEvent.personalnr.split("-")[1];
        newEvent.personalnr = Number(newEvent.personalnr.split("-")[2]);
      }

      const update = await updateEvent(event.id, newEvent)
        .then((res) => res.data)
        .catch((err) => alert(err.message));

      schedulerData.moveEvent(event, slotId, slotName, mappedStartDate, mappedEndDate);
      dispatch({ type: "UPDATE_SCHEDULER", payload: schedulerData });
    }
  };
  const onSetAddMoreState = (newState) => {
    if (newState === undefined) {
      setPopoverState({
        headerItem: undefined,
        left: 0,
        top: 0,
        height: 0,
      });
    } else {
      setPopoverState({
        ...newState,
      });
    }
  };

  const toggleExpandFunc = (schedulerData, slotId) => {
    schedulerData.toggleExpandStatus(slotId);
    dispatch({ type: "UPDATE_SCHEDULER", payload: schedulerData });
  };

  let popover = <div />;
  if (popoverState.headerItem !== undefined) {
    popover = (
      <AddMorePopover
        headerItem={popoverState.headerItem}
        eventItemClick={eventClicked}
        schedulerData={state.viewModel}
        closeAction={onSetAddMoreState}
        left={popoverState.left}
        top={popoverState.top}
        height={popoverState.height}
        moveEvent={moveEvent}
      />
    );
  }

  const nonAgendaCellHeaderTemplateResolver = (
    schedulerData,
    item,
    formattedDateItems,
    style
  ) => {


    return (
      <th key={item.time} className={`header3-text`} style={style}>
        {formattedDateItems.map((formattedItem, index) => {
          if (schedulerData.viewType == 0) {
            return (
              <div>
                <h3 className="text-lg fw-bold">{formattedItem}</h3>
              </div>
            );
          }
          return (
            <div>
              <h3 className="text-lg fw-bold">{formattedItem.split("/")[0].replace(/\b0+(\d+)\b/g, (match, p1) => parseInt(p1))} </h3>
            </div>
          );
        })}
      </th>
    );
  };

 const  eventItemPopoverTemplateResolver = (schedulerData, eventItem, title, start, end, statusColor) => {
    return (
        <div>
            <div className="flex gap-3 p-2 justify items-center">
                <div>
                    <div className="status-dot" style={{backgroundColor: statusColor}} />
                </div>
                <div className="overflow-text">
                    <p className="text-lg fw-bold" title={title}>{title}</p>
                </div>
            </div>
            <div className="flex gap-2">
                <div>
                <p className="text-base text-gray-500 fw-bold">{moment(eventItem.start).format('YYYY-MM-DD')} - {moment(eventItem.originalEnd).format('YYYY-MM-DD')}</p>
                </div>
            </div>
            <div className="flex gap-2">
                <div>
                    <div />
                </div>
            </div>
        </div>
    );
}
  const exportData = () => {
    const jsonString = `data:text/json;chatset=utf-8,${encodeURIComponent(
      JSON.stringify(myEvents)
    )}`;
    const link = document.createElement("a");
    link.href = jsonString;
    link.download = `data-${moment().format('YYYY-MM-DD')}.json`;
    link.click();
  };

  return (
    <div className="flex flex-col items-center justify-center w-full h-full">
      {toggleModal && (
        <Modal
          open={setToggleModal}
          setEventTitle={setEventTitle}
          handleSubmit={saveEvent}
          eventTitle={eventTitle}
          eventType={eventType}
          setEventType={setEventType}
          setAbsentType={setAbsentType}
          absentType={absentType}
          myEvents={myEvents}
          setEmployeeName={setEmployeeName}
          setEmployeeID={setEmployeeID}
          employeeID={employeeID}
          isEmptySlot={isEmptySlot}
        />
      )}
      {state.showScheduler && (
        <div>
          <div className="flex flex-col items-start gap-2 px-4 py-8">
            {branch && (
              <div className="flex items-start justify-between w-full">
                <Dropdown handleSelect={handleSelect} dropDownItem={branch} />
                <button
                  className="bg-blue-400 rounded-lg py-2 px-4 text-white"
                  onClick={exportData}
                >
                  Download
                </button>
              </div>
            )}
            <Scheduler
              schedulerData={state.viewModel}
              prevClick={prevClick}
              nextClick={nextClick}
              onSelectDate={onSelectDate}
              onViewChange={onViewChange}
              updateEventStart={updateEventStart}
              updateEventEnd={updateEventEnd}
              moveEvent={moveEvent}
              newEvent={newEvent}
              onSetAddMoreState={onSetAddMoreState}
              toggleExpandFunc={toggleExpandFunc}
              nonAgendaCellHeaderTemplateResolver={
                nonAgendaCellHeaderTemplateResolver
              }
              eventItemPopoverTemplateResolver ={eventItemPopoverTemplateResolver}
            />
            {popover}
          </div>
        </div>
      )}
    </div>
  );
}

export default wrapperFun(AddMore);
