import { NextResponse } from "next/server";
import { db } from "../../../../prisma/db";
import { t_einsatzdaten } from "@prisma/client";

export const GET = async (req, res) => {
    try {
        const id = Number(req.url.split("branch/")[1]);
        if (!id) {
            return NextResponse.json({ message: "Branch id not found", status: 404 }, { status: 404 });
        }

        const events = await db.t_einsatzdaten.findMany({
            where: {
                kostenstelle: id
            }
        });
        let ResourceArray = [];
        Array.from(new Set(events.map((eventItem: t_einsatzdaten) => JSON.stringify({
            id: eventItem.personalnr,
            name: eventItem.persname
        })))).map((eventString: any, index) => {

            let parsedData = JSON.parse(eventString);
            let parent = {
                id: parsedData.id,
                name: parsedData.name
            }
            let child = {
                id: generateRandomId(parsedData),
                name: null,
                parentId: parent.id
            }
            ResourceArray.push(parent, child)
        });
        const eventData = events.map((user: t_einsatzdaten) => {
            let event = {
                id: user.id,
                start: user.datumvon,
                end:GetEndDate(String(user.datumvon),String(user.datumbis)),
                originalEnd:user.datumbis,
                resourceId:user.event_type == "Present" ? user.personalnr : getResourceId(user,ResourceArray),
                title: user.auftragstyp,
                name: user.persname,
                bgColor:
                    user.event_type == "Present"
                        ? "green"
                        : user.reason == "Sick"
                            ? "#ff0000d6"
                            : user.reason === "Vacation" ? "#0000ffa3" : "gray",
            }
            return event
        });

        return NextResponse.json({
            message: "Event retrived sucessfully", data: {
                events: events,
                resources: ResourceArray,
                eventData:eventData
            }
        }, { status: 200 });

    }
    catch (err) {
        return NextResponse.json({ message: "Error occured", error: err.message, status: 500 }, { status: 500 });
    }

};

function getResourceId(event:t_einsatzdaten,ResourceArray:any){
    let resourceId = ResourceArray.filter((item:any)=> item.parentId == event.personalnr)[0].id;
    return resourceId
}

function generateRandomId(parsedData: any) {
    const timestamp = new Date().getTime();
    const randomId = timestamp.toString() + Math.floor(Math.random() * 1000) + '-' + parsedData.name + '-' + parsedData.id;
    return randomId;
}

function GetEndDate(startDate:string, endDate:string){
    const startDateInMilliSecond = new Date(startDate).valueOf();
    const endDateInMilliSecond = new Date(endDate).valueOf();
    const millisecondsInADay = 24 * 60 * 60 * 1000;
    const diffDate = endDateInMilliSecond - startDateInMilliSecond;
    if (diffDate >= 0) {
      const partitionDay = (30000 * 60) / (7 * millisecondsInADay);
      const addedInputDay = partitionDay * diffDate
      const totalTime = startDateInMilliSecond + addedInputDay;
      const returnDate = new Date(totalTime);
      return returnDate
    } else {
      return startDate
    }
  }