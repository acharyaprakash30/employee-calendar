import { NextResponse } from "next/server";
import { db } from "../../../prisma/db";

export const GET = async (req, res) => {

   const branch = await db.$queryRaw`SELECT DISTINCT kostenstelle FROM \`t_einsatzdaten\``;
   // res.status(200).json({ message: "Branches retrived sucessfully" ,data:branches});
   return NextResponse.json({ message: "Branches retrived sucessfully",data:branch }, { status: 200 });
  };