import { NextResponse } from "next/server";
import { db } from "../../../../prisma/db";

export const GET = async (req, res) => {
    try{
        const id = Number(req.url.split("event/")[1]);
        const event = await db.t_einsatzdaten.findUnique({
            where: {
                id:id
            }
        });
    
        return NextResponse.json({ message: "Event retrived sucessfully",data:event }, { status: 200 });

    }
    catch(err){
        return NextResponse.json({ message: "Error occured",error:err.message,status:500 }, { status: 500 });
    }

   };

   export const PATCH = async (req, res) => {
    try{
        const id = Number(req.url.split("event/")[1]);
        const body = await req.json();
        // const {title, type, start, end, cost_center,employee_id,employee_name} = body;
        // const prevEvent = await db.t_einsatzdaten.findUnique({
        //     where: {
        //         id: id
        //     }
        // });
        // if(!prevEvent){
        //     return NextResponse.json({ message: "Event not found",status:404 }, { status: 404 });
        // }
        const event = await db.t_einsatzdaten.update({
            where: {
                id:id
            },
            data:body
        });
        return NextResponse.json({ message: "Event update sucessfully",data:event }, { status: 200 });
    }
    catch(err){
        return NextResponse.json({ message: "Error occured",error:err.message,status:500 }, { status: 500 });
    }

   };