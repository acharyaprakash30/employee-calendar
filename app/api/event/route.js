import { NextResponse } from "next/server";
import { db } from "../../../prisma/db";

export const GET = async (req, res) => {

   const events = await db.employeeEvent.findMany();
   return NextResponse.json({ message: "Events retrived sucessfully",data:events }, { status: 200 });
  };

  export const POST = async (req, res) => {

    try{
        const body = await req.json(); 
        const {auftragstyp, event_type, personalnr,reason, kostenstelle, persname,datumvon,datumbis} = body;
        const event = await db.t_einsatzdaten.create({
          data:{auftragstyp, event_type, personalnr,reason, kostenstelle, persname,datumvon:new Date(datumvon).toISOString(),datumbis:new Date(datumbis).toISOString()} 
        });
        return NextResponse.json({ message: "Branch created sucessfully",data:event }, { status: 200 });
    }
    catch(err){ 
        return NextResponse.json({ message: "Error occured",data:err }, { status: 500 });
    }
  };